package com.ividen.util.localizer

import java.io._
import java.net.URLEncoder

import akka.actor._
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import play.api.libs.json.{JsValue, Json}
import spray.can.Http
import spray.http.HttpMethods._
import spray.http._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.parsing.combinator.RegexParsers
import scala.util.{Failure, Success}

/**
 * Created by barboza
 */

case class Search(dir: File)

case class Parse(file: File)

case class Translate(text: String, index: Int)

case class Translation(text: String, index: Int)

class SearchFileActor(override val isShutdown: Boolean = false) extends ParentActor with ActorLogging {
  override def receive = {
    case Search(dir) =>
      log.debug("Searching dir {} ...", dir.getAbsolutePath)
      val files: Array[File] = listFiles(dir)
      log.debug("Found {} files in dir {}", files.size, dir.getAbsolutePath)
      if (files.isEmpty) stop
      else files.foreach(processFile)
    case x => super.receive(x)
  }

  private def processFile(f: File): Unit =
    if (f.isDirectory)
      watch(context.actorOf(Props(new SearchFileActor(false)))) ! Search(f)
    else watch(context.actorOf(Props[FileProcessor].withDispatcher("parse-dispatcher"))) ! Parse(f)

  private def listFiles(dir: File): Array[File] = dir.listFiles(Localizer.filter) match {
    case null => Array.empty[File]
    case x => x
  }
}

class FileProcessor extends Actor with RussianSentenceParser with ActorLogging {
  var elements: Array[Element] = Array.empty
  var counter: Int = 0
  var file: File = null

  def receive = {
    case Parse(file) =>
      this.file = file
      log.debug("Parsing file {}", file.getAbsolutePath)
      parseAll(text, new FileReader(file)) match {
        case s: Success[List[Element]] => processElements(s.result.toArray)
        case f: Failure => log.error("Error {} parsing {}", f, file.getAbsolutePath); context.stop(self)
      }

    case Translation(text, index) => {
      println(s"${elements(index).value}=${text}")
      elements(index) = SomeElement(text)
      processCounter
    }

    case scala.util.Failure(t) => {
      log.error("Translation error {}", t)
      processCounter
    }
  }

  private def processCounter: Unit = {
    counter -= 1
    if (counter == 0) {
      val fos = new FileOutputStream(file)
      val ps = new PrintStream(fos)
      elements.foreach(x => ps.print(x.value))
      ps.close()
      fos.close()
      context.stop(self)
    }
  }

  private def processElements(a: Array[Element]) = {
    this.elements = a
    val russElements = a.zipWithIndex.filter(x => x._1.isInstanceOf[RussianElement])
    counter = russElements.length
    log.debug("File {} statistic: Total pieces={} , To be translated = {}", file.getAbsolutePath, a.size, counter)
    if (counter == 0) context.stop(self)
    else russElements.foreach(x => askForTranslate(x._1, x._2))
  }

  private def askForTranslate(e: Element, i: Int) = context.actorOf(Props[Translator].withDispatcher("translate-dispatcher")) ! Translate(e.value, i)
}

object Translator {
  val key = "trnsl.1.1.20150925T233354Z.a8bc01353bddb111.72a3f51259f52bbaa23a18c9866e31b616157482"
  val uri = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&text=%s&lang=ru-en"
  implicit val system = Localizer.system
  implicit val duration = Timeout(120 seconds)
}

class Translator extends Actor with ActorLogging {

  import Translator._
  import context.dispatcher

  override def receive: Actor.Receive = {
    case Translate(text, index) => translate(text, index, self, sender)
  }

  private def translate(value: String, index: Int, owner: ActorRef, sender: ActorRef): Unit = request(value).onComplete {
    case f: Failure[_] =>
      sender ! f
      context.stop(owner)
    case Success(r) =>
      val js = Json.parse(r.entity.asString)
      code(js) match {
        case 200 => sender ! Translation(text(js), index)
        case x => sender ! Failure(new Exception(s"Bad response to translate ${value}. Result code is ${x}"))
      }
      context.stop(owner)
  }

  private def code(js: JsValue): Int = (js \ "code").as[Int]

  private def text(js: JsValue): String = (js \ "text").as[Array[String]].apply(0)

  private def request(value: String): Future[HttpResponse] = (IO(Http) ? HttpRequest(GET, createUri(value))).mapTo[HttpResponse]

  private def createUri(value: String): Uri = Uri(uri.format(encode(key), encode(value)))

  private def encode(value: String): String = URLEncoder.encode(value, "UTF-8")
}

class Filters(filters: String) extends FileFilter {
  private val fileFilters = filters.split(';')

  override def accept(f: File): Boolean = f.isDirectory || fileFilters.exists(f.getPath.endsWith)
}

trait ParentActor extends Actor {
  val buffer = ArrayBuffer[ActorRef]()

  override def receive: Actor.Receive = {
    case Terminated(ref) =>
      buffer -= ref
      if (buffer.isEmpty) stop
  }

  def isShutdown: Boolean

  protected def watch(actor: ActorRef): ActorRef = {
    buffer += actor
    context.watch(actor)
    actor
  }

  protected def stop: Unit = {
    context.stop(self)
    if (isShutdown) context.system.shutdown()
  }
}

object Localizer extends App {
  val filter = new Filters(args(1))
  val system = ActorSystem()

  system.actorOf(Props(new SearchFileActor(true))) ! Search(new File(args(0)))
}

trait Element {
  def value: String
}

case class SomeElement(value: String) extends Element

case class RussianElement(value: String) extends Element

trait RussianSentenceParser extends RegexParsers {
  override def skipWhitespace: Boolean = false

  def text: Parser[List[Element]] = rep(elem)

  private def elem: Parser[Element] = rus | others

  private def rus: Parser[RussianElement] = "^[а-яА-Я][а-яА-Я\\.\\,\\(\\) \\-\\_]+".r ^^ RussianElement

  private def others: Parser[SomeElement] = "[^а-яА-Я]+".r ^^ SomeElement
}

