name := """util.localizer"""

version := "1.0"

scalaVersion := "2.11.7"

// Uncomment to use Akka
libraryDependencies ++= {
  val sprayVersion = "1.3.3"
  val akkaVersion = "2.3.11"
  val parserVersion = "1.0.4"
  val playVersion = "2.3.10"
  val scalaTestVersion = "2.2.4"
  val slf4jVersion = "1.6.4"
  val logbackVersion = "1.0.1"
  Seq(
    "com.typesafe.akka"      %% "akka-actor"               % akkaVersion,
    "org.scala-lang.modules" %% "scala-parser-combinators" % parserVersion,
    "io.spray"               %%  "spray-can"               % sprayVersion,
    "io.spray"               %%  "spray-util"              % sprayVersion,
    "io.spray"               %%  "spray-client"            % sprayVersion,
    "io.spray"               %%  "spray-http"              % sprayVersion,
    "com.typesafe.play"      %%  "play"                    % playVersion,
    "org.slf4j"              %   "slf4j-api"               % slf4jVersion,
    "ch.qos.logback"         %   "logback-classic"         % logbackVersion,
    "ch.qos.logback"         %   "logback-core"            % logbackVersion,
    "org.scalatest"          %% "scalatest"                % scalaTestVersion % "test"
  )
}

mainClass in assembly := Some("com.ividen.util.localizer.Localizer")

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}

fork in run := true
